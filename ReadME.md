# Before Executing

* Azure Functions App & Storage Account should be configured in Azure.
* Storage Account connection string should be set inside `local.settings.json` file in `AzureWebJobsStorage` variable.
* Storage Account > Blob Storage service should have container named `images`.
* Storage Account > Table Storage service should have table named `Message`.
* Storage Account > Queue Storage service should have queue named `messages`.