﻿namespace NovaliteServerlessKSS.Model
{
    public class Person
    {
        private readonly string _firstName = "Petar";
        private readonly string _lastName = "Petrovic";

        public Person()
        {
            
        }

        public Person(string firstName)
        {
            _firstName = firstName;
        }

        public Person(string firstName, string lastName)
        {
            _firstName = firstName;
            _lastName = lastName;
        }

        public string FullName => $"{_firstName} {_lastName}";
    }
}
