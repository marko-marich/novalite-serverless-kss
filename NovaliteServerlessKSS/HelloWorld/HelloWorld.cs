using System.Net;
using System.Net.Http;
using System.Threading;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using NovaliteServerlessKSS.Model;

namespace NovaliteServerlessKSS.HelloWorld
{
    public static class HelloWorld
    {
        [FunctionName("HelloWorld")]
        public static HttpResponseMessage Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "hello-world/name/{name}")]HttpRequestMessage req, 
            string name, 
            TraceWriter log)
        {
            log.Info("HelloWorld HTTP trigger function processed a request.");

            var person = new Person(name);

            //Thread.Sleep(3000);

            // With dll library & nugget package
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(JsonConvert.SerializeObject(person))
            };
        }
    }
}
