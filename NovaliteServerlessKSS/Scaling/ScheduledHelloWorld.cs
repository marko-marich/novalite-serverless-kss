using System;
using System.Net.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;

namespace NovaliteServerlessKSS.Scaling
{
    public static class ScheduledHelloWorld
    {
        [FunctionName("ScheduledHelloWorld")]
        public static void Run(
            [TimerTrigger("*/3 * * * * *")]TimerInfo myTimer,
            TraceWriter log)
        {
            var client = new HttpClient();
            const int numberOfHttpRequests = 5; // Use 1000+ to trigger scaling 
            
            for (var i = 0; i < numberOfHttpRequests; i++)
            {
                client.GetAsync("http://novalite-kss-app.azurewebsites.net/api/hello-world/name/Petra");
            }

            log.Info($"ScheduledHelloWorld processed at: {DateTime.Now}");
        }
    }
}
