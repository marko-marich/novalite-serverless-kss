using System.Linq;
using System.Net;
using System.Net.Http;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage.Table;

namespace NovaliteServerlessKSS.CRUD
{
    public static class MessageCrud
    {
        [FunctionName("GetAllMessages")]
        public static HttpResponseMessage GetAll(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "messages")]HttpRequestMessage req, 
            [Table("Message", Connection = "AzureWebJobsStorage")]IQueryable<Message> messages, 
            TraceWriter log)
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(JsonConvert.SerializeObject(messages))
            };
        }

        [FunctionName("GetMessage")]
        public static HttpResponseMessage GetBy(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "messages/{key}")]HttpRequestMessage req,
            string key,
            [Table("Message", Connection = "AzureWebJobsStorage")]IQueryable<Message> messages,
            TraceWriter log)
        {
            return new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new StringContent(JsonConvert.SerializeObject(messages.ToList().SingleOrDefault(m => m.RowKey == key)))
            };
        }

        [FunctionName("CreateMessage")]
        public static async Task<HttpResponseMessage> Create(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "messages")]HttpRequestMessage req,
            [Table("Message", Connection = "AzureWebJobsStorage")]ICollector<Message> messages,
            TraceWriter log)
        {
            Message newMessage = await req.Content.ReadAsAsync<Message>();

            messages.Add(newMessage);

            return new HttpResponseMessage(HttpStatusCode.Created)
            {
                Content = new StringContent(JsonConvert.SerializeObject(messages))
            };
        }

        [FunctionName("CreateOrUpdateMessage")]
        public static HttpResponseMessage CreateOrUpdate(
            [HttpTrigger(AuthorizationLevel.Anonymous, "put", Route = "messages")]Message updateRequest,
            [Table("Message", Connection = "AzureWebJobsStorage")]CloudTable message,
            TraceWriter log)
        {
            TableOperation updateOperation = TableOperation.InsertOrReplace(updateRequest);
            TableResult result = message.Execute(updateOperation);
            return new HttpResponseMessage((HttpStatusCode)result.HttpStatusCode);
        }

        [FunctionName("DeleteMessage")]
        public static async Task<HttpResponseMessage> Delete(
            [HttpTrigger(AuthorizationLevel.Anonymous, "delete", Route = "messages/{key}")]HttpRequestMessage req,
            string key,
            [Table("Message", Connection = "AzureWebJobsStorage")]CloudTable message,
            TraceWriter log)
        {
            var operation = TableOperation.Delete(new TableEntity("General", key) { ETag = "*" });
            await message.ExecuteAsync(operation);

            return req.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}
