using System;
using Microsoft.WindowsAzure.Storage.Table;

namespace NovaliteServerlessKSS
{
    public class Message : TableEntity
    {
        public string MessageContent { get; set; }

        public Message()
        {
            PartitionKey = "General";
        }

        public Message(string messageContent)
        {
            PartitionKey = "General";
            RowKey = Guid.NewGuid().ToString();
            MessageContent = messageContent;
        }
    }
}