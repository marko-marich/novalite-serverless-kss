using System.IO;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;

namespace NovaliteServerlessKSS.Binding
{
    public static class BlobTrigger
    {
        [FunctionName("BlobTrigger")]
        public static void Run(
            [BlobTrigger("images/{name}", Connection = "AzureWebJobsStorage")]Stream myBlob, 
            string name, 
            TraceWriter log)
        {
            log.Info($"BlobTrigger processed image: '{name}' - {myBlob.Length} Bytes");
        }
    }
}
