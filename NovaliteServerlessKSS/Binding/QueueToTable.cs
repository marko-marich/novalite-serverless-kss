using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;

namespace NovaliteServerlessKSS.Binding
{
    public static class QueueToTable
    {
        [FunctionName("QueueToTable")]
        [return: Table("Message")]
        public static Message Run(
            [QueueTrigger("messages", Connection = "AzureWebJobsStorage")]string queueMessage,
            TraceWriter log)
        {
            log.Info($"QueueToTable processed: '{queueMessage}' queue entry ans store it to Message table!");

            return new Message(queueMessage);
        }
    }
}
